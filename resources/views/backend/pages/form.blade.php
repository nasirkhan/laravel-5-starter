<!-- Tab panes -->
<div class="tab-content">

    <div role="tabpanel" class="tab-pane active" id="content-tab">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('slug', 'Slug') !!}
                    {!! Form::text('slug', old('slug'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    {!! Form::label('content', 'Content') !!}
                    {!! Form::textarea('content', old('content'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">

                    {!! Form::label('featured_image', 'Featured Image') !!}

                    <div class="input-group">
                        {!! Form::text('featured_image', old('featured_image'), ['class' => 'form-control', 'id' => 'featured_image']) !!}
                        <span class="input-group-btn">
                            <a id="btn_featured_image" data-input="featured_image" data-preview="holder" class="btn btn-info">
                                <i class="fa fa-folder-open"></i> Browse
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('category', 'Category') !!}
                    {!! Form::text('category', old('category'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('type', 'Page Type') !!}
                    {!! Form::select('type', array(
                    'forum'=>'Forum Post',
                    'resource'=>'Organizer Resource',
                    'notice'=>'Notice',
                    ),old('type') , ['class' => 'form-control'])
                    !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('status', 'Status') !!}
                    {!! Form::select('status', array(
                    '1'=>'Published',
                    '0'=>'Unpublished',
                    '2'=>'Submitted'
                    ), old('status') , ['class' => 'form-control'])
                    !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('published_at', 'Publish Time') !!}
                    {!! Form::text('published_at', old('published_at') , ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="meta-info">
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    {!! Form::label('meta_title', 'Meta Title') !!}
                    {!! Form::text('meta_title', old('meta_title'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    {!! Form::label('meta_keywords', 'Meta Keyword') !!}
                    {!! Form::text('meta_keywords', old('meta_keywords'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('order', 'Order') !!}
                    {!! Form::text('order', old('order'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    {!! Form::label('meta_description', 'Meta Description') !!}
                    {!! Form::text('meta_description', old('meta_description'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    {!! Form::label('meta_og_image', 'meta_og_image') !!}
                    {!! Form::text('meta_og_image', old('meta_og_image'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    {!! Form::label('meta_og_url', 'meta_og_url') !!}
                    {!! Form::text('meta_og_url', old('meta_og_url'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
