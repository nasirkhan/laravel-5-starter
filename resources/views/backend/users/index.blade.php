@extends('backend.layouts.master')

<?php
$module_name_singular = str_singular($module_name);
?>

@section('title')
{{ $title }}
@stop


@section('page_heading')

<h1>
    {{ ucfirst($module_name) }}
    <small>
        List
    </small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Widgets</li>
</ol>

@stop


@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ ucfirst($module_name) }}</h3>

        <div class="box-tools pull-right">
            <a href="{{ route("admin.$module_name.create") }}" class="btn btn-success">
                <i class="fa fa-plus"></i> Create
            </a>

            <a href="{{ route("admin.$module_name.index") }}" class="btn btn-warning">
                <i class="fa fa-list"></i> All
            </a>

            <a href="{{ route("admin.$module_name.list.deactivated") }}" class="btn btn-warning">
                <i class="fa fa-plus"></i> Deactivated
            </a>

            <a href="{{ route("admin.$module_name.list.banned") }}" class="btn btn-warning">
                <i class="fa fa-plus"></i> Banned
            </a>

            <a href="{{ route("admin.$module_name.list.deleted") }}" class="btn btn-warning">
                <i class="fa fa-plus"></i> Deleted
            </a>
            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            Id
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Created At
                        </th>
                        <th>
                            Updated At
                        </th>
                        <th class="text-right">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $$module_name as  $module_name_singular)

                    <tr>
                        <td>
                            {{ $module_name_singular->id }}
                        </td>
                        <td>
                            <a href="{{ route("admin.$module_name.show", $module_name_singular->id) }}" >
                                {{ $module_name_singular->name }}
                            </a>
                        </td>
                        <td>
                            {{ $module_name_singular->email }}
                        </td>
                        <td>
                            {{ $module_name_singular->created_at }}
                        </td>
                        <td>
                            {{ $module_name_singular->updated_at }}
                        </td>
                        <td>
                            <a data-toggle="tooltip" data-placement="top" title="Edit"
                               class="btn btn-primary btn-sm"
                               href="{{ route("admin.$module_name.edit", $module_name_singular->id) }}">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Deactivate User"
                               class="btn btn-warning btn-sm"
                               href="{{ route("admin.$module_name.status", [$module_name_singular->id, 0]) }}">
                                <i class="fa fa-pause"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Ban User"
                               class="btn btn-danger btn-sm"
                               href="{{ route("admin.$module_name.status", [$module_name_singular->id, 2]) }}">
                                <i class="fa fa-remove"></i>
                            </a>

                            {!! Form::open(['method' => 'DELETE', 'class' => 'form-horizontal', 'route' => ["admin.$module_name.destroy",$module_name_singular->id]]) !!}
                            <button data-toggle="tooltip" data-placement="top" title="Delete User"
                               class="btn btn-sm btn-danger" type="submit">
                                <i class="fa fa-trash" ></i>
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>

            {!! $$module_name->render() !!}
        </div>
    </div>
</div>

@stop

@section ('js-after')

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

@stop
