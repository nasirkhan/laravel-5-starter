@extends('backend.layouts.master')

<?php
$module_name_singular = str_singular($module_name);
?>

@section('title')

{{ $title }}

@endsection


@section('page_heading')

Banned {{ ucfirst($module_name) }} List

@endsection


@section('content')

<p>
    <a href="{{ route("admin.$module_name.create") }}" class="btn btn-success">
        <i class="fa fa-plus"></i> Create {{ ucfirst($module_name_singular) }}
    </a>

    <a href="{{ route("admin.$module_name.index") }}" class="btn btn-warning">
        <i class="fa fa-list"></i> All {{ ucfirst($module_name_singular) }}
    </a>

    <a href="{{ route("admin.$module_name.list.deactivated") }}" class="btn btn-warning">
        <i class="fa fa-plus"></i> Deactivated {{ ucfirst($module_name_singular) }}
    </a>

    <a href="{{ route("admin.$module_name.list.banned") }}" class="btn btn-warning">
        <i class="fa fa-plus"></i> Banned {{ ucfirst($module_name_singular) }}
    </a>

    <a href="{{ route("admin.$module_name.list.deleted") }}" class="btn btn-warning">
        <i class="fa fa-plus"></i> Deleted {{ ucfirst($module_name_singular) }}
    </a>
</p>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>
                Id
            </th>
            <th>
                Name
            </th>
            <th>
                Email
            </th>
            <th>
                Created At
            </th>
            <th>
                Updated At
            </th>
            <th>
                Action
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach( $$module_name as  $module_name_singular)

        <tr>
            <td>
                {{ $module_name_singular->id }}
            </td>
            <td>
                <a href="{{ route("admin.$module_name.show", $module_name_singular->id) }}" >
                    {{ $module_name_singular->name }}
                </a>
            </td>
            <td>
                {{ $module_name_singular->email }}
            </td>
            <td>
                {{ $module_name_singular->created_at }}
            </td>
            <td>
                {{ $module_name_singular->updated_at }}
            </td>
            <td>
                <a data-toggle="tooltip" data-placement="top" title="Edit"
                   class="btn btn-primary btn-sm"
                   href="{{ route("admin.$module_name.edit", $module_name_singular->id) }}">
                    <i class="fa fa-wrench"></i> Edit
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Deactivate User"
                   class="btn btn-warning btn-sm"
                   href="{{ route("admin.$module_name.status", [$module_name_singular->id, 0]) }}">
                    <i class="fa fa-pause"></i>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Activate User"
                   class="btn btn-success btn-sm"
                   href="{{ route("admin.$module_name.status", [$module_name_singular->id, 1]) }}">
                    <i class="fa fa-play"></i>
                </a>
            </td>
        </tr>

        @endforeach
    </tbody>
</table>

{!! $$module_name->render() !!}

@endsection

@section ('js-after')

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

@endsection