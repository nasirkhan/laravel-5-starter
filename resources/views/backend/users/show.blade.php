@extends('backend.layouts.master')
<?php
$module_name_singular = str_singular($module_name);
?>

@section('title')

{{ $title }}

@endsection


@section('page_heading')

{{ ucfirst($module_name_singular) }} {{ $module_action }}

@endsection


@section('content')

<p>
    <a href="{{ route("admin.$module_name.index") }}" class="btn btn-success">
        <i class="fa fa-{{ $module_icon }}"></i> {{ ucfirst($module_name_singular) }} List
    </a>
</p>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>
                Id
            </th>
            <th>
                Name
            </th>
            <th>
                Email
            </th>
            <th>
                Created At
            </th>
            <th>
                Updated At
            </th>
            <th>
                Action
            </th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <td>
                {{ $$module_name_singular->id }}
            </td>
            <td>
                {{ $$module_name_singular->name }}
            </td>
            <td>
                {{ $$module_name_singular->email }}
            </td>
            <td>
                {{ $$module_name_singular->created_at->format('M jS Y g:ia') }}
            </td>
            <td>
                {{ $$module_name_singular->updated_at->format('M jS Y g:ia') }}
            </td>
            <td>
                <a data-toggle="tooltip" data-placement="top" title="Edit"
                   class="btn btn-primary btn-sm"
                   href="{{ route("admin.$module_name.edit", $$module_name_singular->id) }}">
                    <i class="fa fa-wrench"></i> Edit
                </a>

                {!! Form::open(['method' => 'DELETE', 'route' => ["admin.$module_name.destroy",$$module_name_singular->id]]) !!}
                <button data-toggle="tooltip" data-placement="top" title="Delete User"
                   class="btn btn-sm btn-danger" type="submit">
                    <i class="fa fa-trash" ></i>
                </button>
                {!! Form::close() !!}


            </td>
        </tr>

    </tbody>
</table>

@foreach($$module_name_singular->providers as $provider)

<ul>
    <li>{{ $provider->id }}</li>
    <li>{{ $provider->provider }}</li>
    <li>{{ $provider->provider_id }}</li>
    <li>{{ $provider->avatar }}</li>
    <li><img src="{{ $provider->avatar }}" class="img-responsive" /></li>
    <li>{{ $provider->created_at }}</li>
    <li>{{ $provider->updated_at }}</li>
</ul>

@endforeach

@endsection

@section ('js-after')

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

@endsection