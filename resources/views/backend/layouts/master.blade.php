<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Default Description')">
        <meta name="author" content="@yield('author', 'Nasir Khan')">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        @yield('before-styles-end')

        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/skins/_all-skins.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/style-backend.css')}}">

        @yield('after-styles-end')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue fixed sidebar-mini">
        <div class="wrapper">
            @include('backend.includes.header')
            @include('backend.includes.sidebar')

            <div class="content-wrapper">

                <section class="content-header">

                    @yield('page_heading')

                    <ol class="breadcrumb">
                        @yield('breadcrumbs')
                    </ol>

                </section>


                <section class="content">
                    @include('flash::message')
                    @yield('content')
                </section>
            </div>

            @include('backend.includes.footer')
        </div>

        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>-->
        <script>window.jQuery || document.write('<script src="{{asset('js/jquery-2.2.3.min.js')}}"><\/script>')</script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>

        @yield('before-scripts-end')

        <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>

        <script src="{{asset('js/app.min.js')}}"></script>

        @yield('after-scripts-end')

        @include('includes.google-analytics')

    </body>
</html>
