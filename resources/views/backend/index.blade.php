@extends('backend.layouts.master')


@section('title')

{{ $title }}

@stop


@section('page_heading')

<h1>
    <i class="{{ $module_icon }}"></i> {{ $page_heading }}
    <small></small>
</h1>
@stop


@section('content')

<div class="alert alert-info">
    admin area
</div>

@can('view-backend')
    <div class="alert alert-warning">
        Visible to users with 'view-backend' permission
    </div>
@endcan


<a class="btn btn-lg btn-success" href="admin/users">
  <i class="fa fa-users fa-2x pull-left"></i> All Users
</a>


<a class="btn btn-lg btn-success" href="admin/roles">
  <i class="fa fa-user-secret fa-2x pull-left"></i> All Roles
</a>


<a class="btn btn-lg btn-success" href="admin/permissions">
  <i class="fa fa-key fa-2x pull-left"></i> All Permissions
</a>

@stop
