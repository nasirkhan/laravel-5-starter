<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                @foreach (Auth::user()->roles as $role)
                <a href="#"><i class="fa fa-circle text-success"></i> {{ $role->label }} </a> <br />
                @endforeach
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">NAVIGATION</li>
            <li class="{{ Active::pattern('admin') }}">
                <a href="{{ route('backend.dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ Active::pattern('admin/posts*') }}">
                <a href="{!!route('admin.posts.index')!!}">
                    <i class="fa fa-file-text-o"></i>
                    <span>
                         Posts
                    </span>
                </a>
            </li>
            <li class="{{ Active::pattern('admin/pages*') }}">
                <a href="{!!route('admin.pages.index')!!}">
                    <i class="fa fa-trello"></i>
                    <span>
                         Pages
                    </span>
                </a>
            </li>
            <li class="{{ Active::pattern('admin/categories*') }}">
                <a href="{!!route('admin.categories.index')!!}">
                    <i class="fa fa-sitemap"></i>
                    <span>
                         Categories
                    </span>
                </a>
            </li>
            <li class="treeview {{ Active::pattern('admin/users*') }} {{ Active::pattern('admin/roles*') }} {{ Active::pattern('admin/permissions*') }}">
                <a href="#">
                    <i class="fa fa-key"></i> <span>Access</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Active::pattern('admin/users*') }}"><a href="{{ route('admin.users.index') }}"><i class="fa fa-circle-o"></i> Users</a></li>
                    <li class="{{ Active::pattern('admin/roles*') }}"><a href="{{ route('admin.roles.index') }}"><i class="fa fa-circle-o"></i> Roles</a></li>
                    <li class="{{ Active::pattern('admin/permissions*') }}"><a href="{{ route('admin.permissions.index') }}"><i class="fa fa-circle-o"></i> Permissions</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>
