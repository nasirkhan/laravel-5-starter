<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="{{ config('app.url') }}" target="_blank">{{ app_name() }}</a>
    </div>
    <!-- Default to the left -->
    {{ config('settings.copyright') }}
</footer>
