@extends('backend.layouts.master')

@section('title')
{{ $module_action }} {{ $module_title }} | {{ app_name() }}
@stop

@section('page_heading')
<h1>
    <i class="{{ $module_icon }}"></i> {{ $module_title }}
    <small>{{ $module_action }}</small>
</h1>
@stop

@section('breadcrumbs')
<li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active"><i class="{{ $module_icon }}"></i> {{ $module_title }}</li>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $module_title }} {{ $module_action }}</h3>
        <div class="box-tools pull-right">
            <a href='{!! route("admin.$module_name.edit", $$module_name_singular->id) !!}' class="btn btn-primary"><i class="fa fa-wrench"></i>  Edit</a>
        </div>

    </div><!-- /.box-header -->
    <div class="box-body">
        <p>
            Public view: {{ $$module_name_singular->slug }}
        </p>
        <p>
            {!! $$module_name_singular->description !!}
        </p>

        <p>
            {!! $$module_name_singular->user->name !!}
        </p>
        <p>
            <pre><?php // var_dump($post); ?></pre>
        </p>
    </div><!-- /.box-body -->
</div><!--box box-success-->
@stop
