@extends('backend.layouts.master')

@section('title')
{{ $module_action }} {{ $module_title }} | {{ app_name() }}
@stop

@section('page_heading')
<h1>
    <i class="{{ $module_icon }}"></i> {{ $module_title }}
    <small>{{ $module_action }}</small>
</h1>
@stop

@section('breadcrumbs')
<li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href='{!!route("admin.$module_name.index")!!}'><i class="{{ $module_icon }}"></i> {{ $module_title }}</a></li>
<li class="active"> {{ $module_action }}</li>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $module_title }} {{ $module_action }}</h3>
        <div class="box-tools pull-right">
            <a href="{{ route("admin.$module_name.show", $$module_name_singular->id) }}" class="btn btn-primary pull-right btn-sm">
                <i class="fa fa-television"></i> {{ 'Show ' }}
            </a>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">

        @include ('errors.list')

        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#content-tab" aria-controls="content-tab" role="tab" data-toggle="tab">
                        Content
                    </a>
                </li>
            </ul>

            {!! Form::model($$module_name_singular, ['method' => 'PATCH', 'url' => ["admin/$module_name", $$module_name_singular->id], 'files' => true, 'class' => 'form']) !!}

            {!! csrf_field() !!}

            @include ("backend.$module_name.form")



            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        {!! Form::button("<i class='fa fa-save'></i> Save", ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                <div class="col-sm-8">
                    <div class="pull-right">
                        {!! Form::model($$module_name_singular, ['method' => 'delete', 'url' => ["admin/$module_name", $$module_name_singular->id]]) !!}
                        <div class="form-group">
                            {!! Form::button("<i class='fa fa-trash'></i>", ['class' => 'btn btn-danger', 'type'=>'submit']) !!}

                            <a class="btn btn-warning" href="{{ route("admin.$module_name.index") }}">
                                <i class="fa fa-reply"></i> Cancel
                            </a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>

    </div><!-- /.box-body -->
</div><!--box box-success-->
@stop


@section('after-scripts-end')

<script type="text/javascript" src="{{ asset('/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="/vendor/laravel-filemanager/js/lfm.js"></script>

<script type="text/javascript">

$('#btn_featured_image').filemanager('image');


CKEDITOR.replace( 'content', {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
});

</script>

@stop
