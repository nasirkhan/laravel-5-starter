@extends('backend.layouts.master')

@section('title')
{{ $module_action }} {{ $module_title }} | {{ app_name() }}
@stop

@section('page_heading')
<h1>
    <i class="{{ $module_icon }}"></i> {{ $module_title }}
    <small>{{ $module_action }}</small>
</h1>
@stop

@section('breadcrumbs')
<li><a href="{!!route('backend.dashboard')!!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active"><i class="{{ $module_icon }}"></i> {{ $module_title }}</li>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $module_title }} {{ $module_action }}</h3>
        <div class="box-tools pull-right">
            <div class="pull-right mb-10">
                <div class="btn-group">
                    <a href="{{ route("admin.$module_name.create") }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus"></i> Create
                    </a>
                </div>

                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-cog"></i>  Options <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route("admin.$module_name.trashed") }}">
                                <i class="fa fa-eye-slash"></i> View trash
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">

        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table id="datatable" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Category
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                Updated At
                            </th>
                            <th>
                                Created By
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($$module_name as $module_name_singular)
                        <tr>
                            <td>
                                {{ $module_name_singular->id }}
                            </td>
                            <td>
                                <a href="{{ url("admin/$module_name", $module_name_singular->id) }}">{{ $module_name_singular->title }}</a>
                                <br />
                                {{ $module_name_singular->slug }}
                            </td>
                            <td>
                                {{ $module_name_singular->category }}
                            </td>
                            <td>
                                {{ $module_name_singular->type }}
                            </td>
                            <td>
                                {{ $module_name_singular->updated_at }}
                            </td>
                            <td>
                                {{ $module_name_singular->user->name }}
                            </td>
                            <td>
                                {{ $module_name_singular->status }}
                            </td>
                            <td>
                                <a href='{!!route("admin.$module_name.edit", $module_name_singular->id)!!}' class='btn btn-primary btn-sm'><i class="fa fa-wrench"></i> Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div><!-- /.box-body -->
</div><!--box box-success-->
@stop
