@extends('frontend.layouts.master')

@section('jumbotron')
<div class="jumbotron">
    <div class="container">
        <h1>User Dashboard</h1>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <a class="btn btn-default" href="{{ route('profiles.show', Auth::user()->id) }}" role="button">User Profile</a>
        <a class="btn btn-default" href="{{ route('password.change') }}" role="button">Change Password</a>
    </div>
</div>
@endsection