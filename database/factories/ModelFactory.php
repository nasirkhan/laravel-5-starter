<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Post::class, function (Faker\Generator $faker) {

    $user = Auth::loginUsingId(1);

    return [
        'title' => $faker->sentence,
        'content' => $faker->paragraph,
        'type' => $faker->randomElement($array = array ('Type A','Type B','Type C')),
        'category' => $faker->randomElement($array = array ('Category A','Category B','Category C','Category D','Category E')),
        'featured_image' => 'https://unsplash.it/500/300/',
        'published_at' => $faker->dateTimeThisMonth(),
        'created_by' => $user->id,
    ];
});
