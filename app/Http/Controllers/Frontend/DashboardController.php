<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function __construct() {
        $this->module_name = 'dashboards';
        $this->module_icon = 'dashboard';
        $this->title = "Application User Dashboard";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $title = $this->title;
        $module_name = $this->module_name;
        $module_icon = $this->module_icon;

        $page_heading = "User Dashboard";

        if (auth()->user()->can('permission1')) {
            return "omg... it worked";
        }


        return view('frontend.dashboard.index', compact('title', 'page_heading', 'module_icon'));
    }
}
