<?php

//users routes
Route::get('users/deactivated', [
    'as' => 'admin.users.list.deactivated',
    'uses' => 'UsersController@deactivated']);

Route::get('users/banned', [
    'as' => 'admin.users.list.banned',
    'uses' => 'UsersController@banned']);

Route::get('users/deleted', [
    'as' => 'admin.users.list.deleted',
    'uses' => 'UsersController@deleted']);

Route::post('users/deleted/{id}', [
    'as' => 'admin.users.restore',
    'uses' => 'UsersController@restore']);

Route::resource('users', 'UsersController');

Route::get('users/{user_id}/status/{status}', [
    'as' => 'admin.users.status',
    'uses' => 'UsersController@status'])->where(['user_id' => '[0-9]+', 'status' => '[0,1,2]']);

// users with administrator role can only access this route
Route::group(['middleware' => 'role:administrator'], function () {
    Route::resource('roles', 'RolesController');
});

// users with view-permissions permission can only access this route
Route::group(['middleware' => 'permission:view-permissions'], function () {
    Route::resource('permissions', 'PermissionsController');
});
