<?php

Route::group(['middleware' => 'guest'], function () {
    // Authentication routes
    Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('auth/login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);

    // Registration routes
    Route::get('auth/register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
    Route::post('auth/register', ['as' => 'register', 'uses' => 'Auth\AuthController@postRegister']);
    Route::get('register/confirm/{token}', ['as' => 'register.confirm', 'uses' => 'Auth\AuthController@confirmEmail']);

    // Password reset link request routes
    Route::get('password/email', ['as' => 'password.email', 'uses' => 'Auth\PasswordController@getEmail']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\PasswordController@postEmail']);

    // Password reset routes
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');
});


Route::group(['middleware' => 'auth'], function () {
    // logout
    Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

    // Password change
    Route::get('auth/password/change', ['as' => 'password.change', 'uses' => 'Backend\UsersController@getChangePassword']);
    Route::post('auth/password/change', ['as' => 'password.change', 'uses' => 'Backend\UsersController@postChangePassword']);
});

// Socialite routes
Route::get('auth/{provider}', ['as' => 'social.login', 'uses' => 'Auth\AuthController@redirectToProvider']);
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

